"""An OpenStack Python Pulumi program"""

import pulumi
from pulumi_openstack import compute


def createPetite(name):
	cloud_init=open("user-data", mode='r').read()
	# Create an OpenStack resource (Compute Instance)
	instance = compute.Instance(name,flavor_name='petite',	image_name='ubuntu22.04',	user_data=cloud_init)
	pulumi.export(name+'_ip', instance.access_ip_v4)

def createPostgre(name):
	cloud_init=open("user-data-postgresql", mode='r').read()
	# Create an OpenStack resource (Compute Instance)
	instance = compute.Instance(name,flavor_name='normale',	image_name='ubuntu22.04',	user_data=cloud_init)
	pulumi.export(name+'_ip', instance.access_ip_v4)

# Export the IP of the instance
createPostgre("postgre")
createPetite("client1")
createPetite("client2")