#!/home/infoetu/corentin.sotoca.etu/Documents/r5b_04/TP03/TP03/bin/python3
import fabric,argparse

salles={"ayou":26,"bouleau":26,"meleze":26,"teck":26,"frene":26,"acajou":26,"epicea":26,"hevea":26,"saule":26,"if":14,"sequoya":8,"hetre":8}

# Args:
# ----------------------
parser = argparse.ArgumentParser()
parser.add_argument("-s", help="Nom de la salle")
parser.add_argument("-l", help="Précise un utilisateur a vérifier")
parser.add_argument("--size", help="Affiche la taille occupé par la machine", action="store_true")
args=parser.parse_args()
# print(args)

def iterateSalle(salle,login,size_b):
    hosts = [ f"{str(salle)}{nb:02d}" for nb in range(1, salles.get(salle)+1) ]
    fabric.Connection(hosts[0]).put("on_machine.py","on_machine.py")
    salle = fabric.SerialGroup(*hosts)
    #salle.put("on_machine.py")
    salle.run(f"echo -e \"----------------\n$HOSTNAME\n----------------\";python -c 'from on_machine import *; find_boxes(\"{login}\",{size_b})'")
    fabric.Connection(hosts[0]).run("rm on_machine.py")

    

# Choix de la salle
# ----------------------
if(args.s == None):
    for salle in salles.keys():
        iterateSalle(salle,args.l,args.size)
elif(salles.get(args.s) != None): 
    iterateSalle(args.s,args.l,args.size)
else:
    print("Veuillez entrer un bon argument pour le nom de la salle ou le laisser vide. La salle "+args.s+" n'existe pas.")

