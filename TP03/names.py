#!/home/infoetu/corentin.sotoca.etu/Documents/r5b_04/TP03/TP03/bin/python3
from fabric import SerialGroup as Group

hosts = [ f"teck{nb:02d}" for nb in range(1, 27) ]
salle = Group(*hosts)
salle.run('hostname')