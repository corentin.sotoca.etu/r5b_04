#!/home/infoetu/corentin.sotoca.etu/Documents/r5b_04/TP03/TP03/bin/python3
from pathlib import Path
import os

vdi = []
def find_boxes(USER=None, size=True):
    if USER==None or USER=="None":
        USER= os.getlogin()
    path= Path(f"/usr/local/virtual_machine/infoetu/{USER}/")
    vdi = [file for file in path.glob('**/*') if file.suffix in ('.vdi', '.vmdk')]
    for file in vdi:
        if(size == True):
            taille = (file.stat().st_size) / 1024 / 1024 / 1024
            print(f"- {file.parent.name}/{file.name} ({taille:.2f} Go)")
        else:
            print(f"- {file.parent.name}/{file.name}")
