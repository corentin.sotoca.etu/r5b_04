#!/home/infoetu/corentin.sotoca.etu/Documents/r5b_04/TP04/.env/bin/python
from jinja2 import *
import os, ipaddress, json, sys, subprocess

labPath="./lab"
inputFile="./input.json"


def jsonToObject(path):
    file=open(path, mode='r')
    obj=json.load(file)
    return obj

def createMachine(machine):
    machinePath=labPath+"/"+machine.get("device")
    os.makedirs(machinePath,exist_ok=True)
    if(machine.get("type")=="router"):
        os.makedirs(machinePath+"/etc/frr",exist_ok=True)
        createFrrConf(machine.get("interfaces"),machine.get("device"),machine.get("routing"))
        createDaemon(machine.get("device"),machine.get("routing"))
        createRTRStartup(machine.get("device"))
    else:
        os.makedirs(machinePath+"/etc/network",exist_ok=True)
        createNetInterfaces(machine.get("interfaces"),machine.get("device"))
        createPCStartup(machine.get("interfaces"),machine.get("device"))

def createNetInterfaces(interfaces,deviceName):
    loader = FileSystemLoader("./res/pc")
    environment = Environment(loader=loader)
    template = environment.get_template("interfaces.tpl")
    result = template.render(interfaces=interfaces)
    f = open(labPath+"/"+deviceName+"/etc/network/interfaces",mode='w')
    f.write(result)

def createFrrConf(interfaces,deviceName,routing):
    loader = FileSystemLoader("./res/rtr")
    environment = Environment(loader=loader)
    template = environment.get_template("frr.conf.tpl")
    result = template.render(interfaces=interfaces,deviceName=deviceName,routing=routing)
    f = open(labPath+"/"+deviceName+"/etc/frr/frr.conf",mode='w')
    f.write(result)

def createDaemon(deviceName,routing):
    loader = FileSystemLoader("./res/rtr")
    environment = Environment(loader=loader)
    template = environment.get_template("daemons.tpl")
    result = template.render(routing=routing)
    f = open(labPath+"/"+deviceName+"/etc/frr/daemons",mode='w')
    f.write(result)

def createPCStartup(interfaces, deviceName):
    loader = FileSystemLoader("./res/pc")
    environment = Environment(loader=loader)
    template = environment.get_template("startup.tpl")
    result = template.render(interfaces=interfaces)
    f = open(labPath+"/"+deviceName+".startup",mode='w')
    f.write(result)

def createRTRStartup(deviceName):
    rtrStartup="service frr start"
    f = open(labPath+"/"+deviceName+".startup",mode='w')
    f.write(rtrStartup)

def toNetworkAddress(machines):
    network={}
    idx=97
    for machine in machines:
        for interface in machine.get("interfaces"):
            if(interface.get("ipv4_address")!=None):
                netAddress=str(ipaddress.ip_network(interface.get("ipv4_address"),strict=False))
                if(network.get(netAddress)==None):
                    network.update({netAddress:chr(idx)})
                    idx=idx+1
                print(network)
                interface.update({"ipv4_address":network.get(netAddress)})

    return machines

def createLabConf(machines,network):
    loader = FileSystemLoader("./res")
    environment = Environment(loader=loader)
    template = environment.get_template("lab.conf.tpl")
    result = template.render(machines=machines)
    f = open(labPath+"/lab.conf",mode='w')
    f.write(result)


obj=jsonToObject(inputFile)

subprocess.run(["rm", "-rf", labPath])

# On crée les machine finito pipo
images={}
network={}
idx=97

for item in obj:
    createMachine(item)
    if item.get("type") == "router":
        images.update({item.get("device"):item.get("image")})
print(obj)
obj2=toNetworkAddress(obj)
print("---------------------------------\n\n")
print(obj2)

print("---------------------------------\n\n")
# On configure le fichier lab.conf
createLabConf(obj2,network)
