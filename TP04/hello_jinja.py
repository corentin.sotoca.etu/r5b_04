#!/home/infoetu/corentin.sotoca.etu/Documents/r5b_04/TP04/.env/bin/python
from jinja2 import Environment

user = {"nom": "Peter", "prenom": "Yvan"}
environment = Environment()
template = environment.from_string("Hello {{prenom }} {{nom| upper}}")

print(template.render(user))