LAB_DESCRIPTION="{{description}}"
LAB_VERSION=1.0
LAB_AUTHOR="{{author}}"

{% for machine in machines %}
{% if machine.type == "router" %}
{{machine.device}}[image]="{{machine.image}}"
{% endif %}
{% endfor %}

# Topologie
{% for machine in machines %}
{% for interface in machine.interfaces %}
{% if interface.up == "true"%}
{{machine.device}}[{{loop.index0}}]={{interface.ipv4_address}}
{% endif %}
{% endfor %}
{% endfor %}