{% for interface in interfaces %}
{% if interface.up == "true" %}
auto {{interface.name}}

iface {{interface.name}} inet static
    address {{interface.ipv4_address}}
    gateway {{interface.gateway}}
{% endif %}
{% endfor %}