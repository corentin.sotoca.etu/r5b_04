{% for interface in interfaces %}
{% if interface.up == "true" %}
ifup {{interface.name}}
{% else %}
ifdown {{interface.name}}
{% endif %}
{% endfor %}