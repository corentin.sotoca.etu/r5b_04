hostname {{deviceName}}

{% for interface in interfaces %}
{% if interface.up == "true" %}
interface {{interface.name}}
ip address {{interface.ipv4_address}}
ipv6 address {{interface.ipv6_address}}
{% if "ospf" is in routing %}
ip ospf area 0
{% endif %}
{% if "ospfv6" is in routing %}
ipv6 ospf6 area 0
{% endif %}
{% endif %}
{% endfor %}

{% if "ospf" is in routing %}
router ospf
ospf router-id {{interfaces[0].ipv4_address}}
redistribute static
{% endif %}

{% if "ospfv6" is in routing %}
router ospf6
ospf6 router-id {{interfaces[0].ipv4_address}}
{% endif %}
