#!/usr/bin/python3
import sys, grp

def get_lists(which):
    groupList=[]
    userList=[]
    for group in grp.getgrall():
        groupList.append(group.gr_name)
        for user in group.gr_mem:
            if( user not in userList ):
                userList.append(user) 
    if(which == "group"):
        return groupList
    elif (which == "user"):
        return userList
    else:
        return []

if __name__ == '__main__':
    print(get_lists(sys.argv[1]))
