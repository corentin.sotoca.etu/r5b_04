#!/usr/bin/python3
import sys, grp, subprocess, csv, createUser

results = []
with open(sys.argv[1]) as csvfile:
    reader = csv.reader(csvfile,delimiter=",") # change contents to floats
    for row in reader: # each row is a list
        results.append(row)

for line in results:
    prenom=line[1].split(' ')[0]
    nom=line[1].split(' ')[1]
    group=line[2]
    tab = [prenom,nom,group]
    createUser.new(tab)


