#!/usr/bin/python3
import sys, grp

def get_user_groups_by_name(login):
    """
    Renvoie la liste des groupes d'un utilisateur

    Args:
        user (str): un login utilisateur
    Returns:
        [str]: la liste des groupes auquels il appartient
    """
    groups = [ u.gr_name for u in grp.getgrall() if login in u.gr_mem ]
    
    return groups

if __name__ == '__main__':
    print(get_user_groups_by_name(sys.argv[1]))
