#!/usr/bin/python3
import sys, grp, subprocess, getGroupMembers

def new (argv):

    if(len(argv) < 2):
        print("T'es nul")
        exit(1)
    elif(len(argv) < 3):
        # login
        subprocess.run(["sudo","useradd",argv[1]])
        subprocess.run(["sudo","usermod","-aG",argv[2].lower(),argv[1]])
    elif(len(argv) < 4):
        # Prenom + Nom
        login=(argv[0][0]+argv[1]).lower()
        subprocess.run(["sudo","useradd",login])
        if(len(getGroupMembers.get_group_members_by_name(argv[2])) < 1):
            subprocess.run(["sudo", "groupadd",argv[2]])
        subprocess.run(["sudo","usermod","-aG",argv[2].lower(),login])
    else:
        print("T'es vraiment nul")

if __name__ == '__main__':
    print(new(sys.argv))
