#!/usr/bin/python3
import sys, grp

def get_group_members_by_name(group):
    """
    Renvoie la liste des logins utilisateurs appartenant au groupe

    Args:
        group (str): un nom de groupe

    Returns:
        [str]: la liste des logins utilisateurs du groupe
    """
    try:
        members = grp.getgrnam(group).gr_mem
    except KeyError:
        return []
    return members


if __name__ == '__main__':
    print(get_group_members_by_name(sys.argv[1]))
